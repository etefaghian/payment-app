package ir.etefaghian.paymentapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

/**
 * A PredictMatch.
 */
@Entity
@Table(name = "predict_match")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PredictMatch implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(name = "predicted_goal_1")
    private Integer predictedGoal1;

    @Column(name = "predicted_goal_2")
    private Integer predictedGoal2;

    @ManyToOne
    @JsonIgnoreProperties("predictMatches")
    private User userId;

    @ManyToOne
    @JsonIgnoreProperties("predictMatches")
    private Match matchId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public Integer getPredictedGoal1() {
        return predictedGoal1;
    }

    public PredictMatch predictedGoal1(Integer predictedGoal1) {
        this.predictedGoal1 = predictedGoal1;
        return this;
    }

    public void setPredictedGoal1(Integer predictedGoal1) {
        this.predictedGoal1 = predictedGoal1;
    }

    public Integer getPredictedGoal2() {
        return predictedGoal2;
    }

    public PredictMatch predictedGoal2(Integer predictedGoal2) {
        this.predictedGoal2 = predictedGoal2;
        return this;
    }

    public void setPredictedGoal2(Integer predictedGoal2) {
        this.predictedGoal2 = predictedGoal2;
    }

    public User getUserId() {
        return userId;
    }

    public PredictMatch userId(User user) {
        this.userId = user;
        return this;
    }

    public void setUserId(User user) {
        this.userId = user;
    }

    public Match getMatchId() {
        return matchId;
    }

    public PredictMatch matchId(Match match) {
        this.matchId = match;
        return this;
    }

    public void setMatchId(Match match) {
        this.matchId = match;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PredictMatch)) {
            return false;
        }
        return id != null && id.equals(((PredictMatch) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "PredictMatch{" +
            "id=" + getId() +
            ", matchId='" + getMatchId() + "'" +
            ", userId='" + getUserId() + "'" +
            ", predictedGoal1=" + getPredictedGoal1() +
            ", predictedGoal2=" + getPredictedGoal2() +
            "}";
    }
}
