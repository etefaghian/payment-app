package ir.etefaghian.paymentapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

/**
 * A TransactionOfAutoWrongDoing.
 */
@Entity
@Table(name = "awd")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TransactionOfAutoWrongDoing implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "car_number")
    private String carNumber;

    @Column(name = "amount")
    private String amount;

    @Column(name = "date")
    private Instant date;

    @Column(name = "receipt")
    private String receipt;

    @Column(name = "is_done")
    private Boolean isDone;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JsonIgnoreProperties("transactionOfAutoWrongDoings")
    private User userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }




    public String getCardNumber() {
        return cardNumber;
    }

    public TransactionOfAutoWrongDoing cardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
        return this;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public TransactionOfAutoWrongDoing carNumber(String carNumber) {
        this.carNumber = carNumber;
        return this;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getAmount() {
        return amount;
    }

    public TransactionOfAutoWrongDoing amount(String amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Instant getDate() {
        return date;
    }

    public TransactionOfAutoWrongDoing date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public String getReceipt() {
        return receipt;
    }

    public TransactionOfAutoWrongDoing receipt(String receipt) {
        this.receipt = receipt;
        return this;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public Boolean isIsDone() {
        return isDone;
    }

    public TransactionOfAutoWrongDoing isDone(Boolean isDone) {
        this.isDone = isDone;
        return this;
    }

    public void setIsDone(Boolean isDone) {
        this.isDone = isDone;
    }

    public String getDescription() {
        return description;
    }

    public TransactionOfAutoWrongDoing description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUserId() {
        return userId;
    }

    public TransactionOfAutoWrongDoing userId(User user) {
        this.userId = user;
        return this;
    }

    public void setUserId(User user) {
        this.userId = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TransactionOfAutoWrongDoing)) {
            return false;
        }
        return id != null && id.equals(((TransactionOfAutoWrongDoing) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TransactionOfAutoWrongDoing{" +
            "id=" + getId() +
            ", userId='" + getUserId() + "'" +
            ", cardNumber='" + getCardNumber() + "'" +
            ", carNumber='" + getCarNumber() + "'" +
            ", amount='" + getAmount() + "'" +
            ", date='" + getDate() + "'" +
            ", receipt='" + getReceipt() + "'" +
            ", isDone='" + isIsDone() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
