package ir.etefaghian.paymentapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

/**
 * A TransactionOfWallet.
 */
@Entity
@Table(name = "transaction_of_wallet")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TransactionOfWallet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;




    @Column(name = "amount")
    private Long amount;

    @Column(name = "date")
    private Instant date;

    @Column(name = "is_done")
    private Boolean isDone;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JsonIgnoreProperties("transactionOfWallets")
    private Wallet sourceWalletId;

    @ManyToOne
    @JsonIgnoreProperties("transactionOfWallets")
    private Wallet destinationWalletId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getAmount() {
        return amount;
    }

    public TransactionOfWallet amount(Long amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Instant getDate() {
        return date;
    }

    public TransactionOfWallet date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Boolean isIsDone() {
        return isDone;
    }

    public TransactionOfWallet isDone(Boolean isDone) {
        this.isDone = isDone;
        return this;
    }

    public void setIsDone(Boolean isDone) {
        this.isDone = isDone;
    }

    public String getDescription() {
        return description;
    }

    public TransactionOfWallet description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Wallet getSourceWalletId() {
        return sourceWalletId;
    }

    public TransactionOfWallet sourceWalletId(Wallet wallet) {
        this.sourceWalletId = wallet;
        return this;
    }

    public void setSourceWalletId(Wallet wallet) {
        this.sourceWalletId = wallet;
    }

    public Wallet getDestinationWalletId() {
        return destinationWalletId;
    }

    public TransactionOfWallet destinationWalletId(Wallet wallet) {
        this.destinationWalletId = wallet;
        return this;
    }

    public void setDestinationWalletId(Wallet wallet) {
        this.destinationWalletId = wallet;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TransactionOfWallet)) {
            return false;
        }
        return id != null && id.equals(((TransactionOfWallet) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TransactionOfWallet{" +
            "id=" + getId() +
            ", sourceWalletId='" + getSourceWalletId() + "'" +
            ", destinationWalletId='" + getDestinationWalletId() + "'" +
            ", amount=" + getAmount() +
            ", date='" + getDate() + "'" +
            ", isDone='" + isIsDone() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
