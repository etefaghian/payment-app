package ir.etefaghian.paymentapp.domain;

import ir.etefaghian.paymentapp.domain.enumeration.TypeOfService;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

/**
 * A ScoreTable.
 */
@Entity
@Table(name = "score_table")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ScoreTable implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_of_service")
    private TypeOfService typeOfService;

    @Column(name = "score")
    private Long score;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeOfService getTypeOfService() {
        return typeOfService;
    }

    public ScoreTable typeOfService(TypeOfService typeOfService) {
        this.typeOfService = typeOfService;
        return this;
    }

    public void setTypeOfService(TypeOfService typeOfService) {
        this.typeOfService = typeOfService;
    }

    public Long getScore() {
        return score;
    }

    public ScoreTable score(Long score) {
        this.score = score;
        return this;
    }

    public void setScore(Long score) {
        this.score = score;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ScoreTable)) {
            return false;
        }
        return id != null && id.equals(((ScoreTable) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ScoreTable{" +
            "id=" + getId() +
            ", typeOfService='" + getTypeOfService() + "'" +
            ", score=" + getScore() +
            "}";
    }
}
