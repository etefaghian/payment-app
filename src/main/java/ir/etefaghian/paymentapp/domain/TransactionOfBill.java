package ir.etefaghian.paymentapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

/**
 * A TransactionOfBill.
 */
@Entity
@Table(name = "transaction_of_bill")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TransactionOfBill implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "bill_number")
    private String billNumber;

    @Column(name = "payment_number")
    private String paymentNumber;

    @Column(name = "amount")
    private String amount;

    @Column(name = "date")
    private Instant date;

    @Column(name = "receipt")
    private String receipt;

    @Column(name = "is_done")
    private Boolean isDone;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JsonIgnoreProperties("transactionOfBills")
    private User userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }




    public String getCardNumber() {
        return cardNumber;
    }

    public TransactionOfBill cardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
        return this;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public TransactionOfBill billNumber(String billNumber) {
        this.billNumber = billNumber;
        return this;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getPaymentNumber() {
        return paymentNumber;
    }

    public TransactionOfBill paymentNumber(String paymentNumber) {
        this.paymentNumber = paymentNumber;
        return this;
    }

    public void setPaymentNumber(String paymentNumber) {
        this.paymentNumber = paymentNumber;
    }

    public String getAmount() {
        return amount;
    }

    public TransactionOfBill amount(String amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Instant getDate() {
        return date;
    }

    public TransactionOfBill date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public String getReceipt() {
        return receipt;
    }

    public TransactionOfBill receipt(String receipt) {
        this.receipt = receipt;
        return this;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public Boolean isIsDone() {
        return isDone;
    }

    public TransactionOfBill isDone(Boolean isDone) {
        this.isDone = isDone;
        return this;
    }

    public void setIsDone(Boolean isDone) {
        this.isDone = isDone;
    }

    public String getDescription() {
        return description;
    }

    public TransactionOfBill description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUserId() {
        return userId;
    }

    public TransactionOfBill userId(User user) {
        this.userId = user;
        return this;
    }

    public void setUserId(User user) {
        this.userId = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TransactionOfBill)) {
            return false;
        }
        return id != null && id.equals(((TransactionOfBill) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TransactionOfBill{" +
            "id=" + getId() +
            ", userId='" + getUserId() + "'" +
            ", cardNumber='" + getCardNumber() + "'" +
            ", billNumber='" + getBillNumber() + "'" +
            ", paymentNumber='" + getPaymentNumber() + "'" +
            ", amount='" + getAmount() + "'" +
            ", date='" + getDate() + "'" +
            ", receipt='" + getReceipt() + "'" +
            ", isDone='" + isIsDone() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
