package ir.etefaghian.paymentapp.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

/**
 * A Match.
 */
@Entity
@Table(name = "jhi_match")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Match implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "team_1_name")
    private String team1name;

    @Column(name = "team_2_name")
    private String team2name;

    @Column(name = "team_1_goal")
    private String team1Goal;

    @Column(name = "team_2_goal")
    private String team2Goal;

    @Column(name = "date")
    private Instant date;

    @Column(name = "is_done")
    private Boolean isDone;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTeam1name() {
        return team1name;
    }

    public Match team1name(String team1name) {
        this.team1name = team1name;
        return this;
    }

    public void setTeam1name(String team1name) {
        this.team1name = team1name;
    }

    public String getTeam2name() {
        return team2name;
    }

    public Match team2name(String team2name) {
        this.team2name = team2name;
        return this;
    }

    public void setTeam2name(String team2name) {
        this.team2name = team2name;
    }

    public String getTeam1Goal() {
        return team1Goal;
    }

    public Match team1Goal(String team1Goal) {
        this.team1Goal = team1Goal;
        return this;
    }

    public void setTeam1Goal(String team1Goal) {
        this.team1Goal = team1Goal;
    }

    public String getTeam2Goal() {
        return team2Goal;
    }

    public Match team2Goal(String team2Goal) {
        this.team2Goal = team2Goal;
        return this;
    }

    public void setTeam2Goal(String team2Goal) {
        this.team2Goal = team2Goal;
    }

    public Instant getDate() {
        return date;
    }

    public Match date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Boolean isIsDone() {
        return isDone;
    }

    public Match isDone(Boolean isDone) {
        this.isDone = isDone;
        return this;
    }

    public void setIsDone(Boolean isDone) {
        this.isDone = isDone;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Match)) {
            return false;
        }
        return id != null && id.equals(((Match) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Match{" +
            "id=" + getId() +
            ", team1name='" + getTeam1name() + "'" +
            ", team2name='" + getTeam2name() + "'" +
            ", team1Goal='" + getTeam1Goal() + "'" +
            ", team2Goal='" + getTeam2Goal() + "'" +
            ", date='" + getDate() + "'" +
            ", isDone='" + isIsDone() + "'" +
            "}";
    }
}
