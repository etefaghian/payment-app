package ir.etefaghian.paymentapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

/**
 * A Wallet.
 */
@Entity
@Table(name = "wallet")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Wallet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;




    @Column(name = "amount")
    private Long amount;

    @ManyToOne
    @JsonIgnoreProperties("wallets")
    private User userId;

    @ManyToOne
    @JsonIgnoreProperties("wallets")
    private Card cardId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }






    public Long getAmount() {
        return amount;
    }

    public Wallet amount(Long amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public User getUserId() {
        return userId;
    }

    public Wallet userId(User user) {
        this.userId = user;
        return this;
    }

    public void setUserId(User user) {
        this.userId = user;
    }

    public Card getCardId() {
        return cardId;
    }

    public Wallet cardId(Card card) {
        this.cardId = card;
        return this;
    }

    public void setCardId(Card card) {
        this.cardId = card;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Wallet)) {
            return false;
        }
        return id != null && id.equals(((Wallet) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Wallet{" +
            "id=" + getId() +
            ", cardId='" + getCardId() + "'" +
            ", userId='" + getUserId() + "'" +
            ", amount=" + getAmount() +
            "}";
    }
}
