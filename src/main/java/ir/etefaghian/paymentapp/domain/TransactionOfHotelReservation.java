package ir.etefaghian.paymentapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

/**
 * A TransactionOfHotelReservation.
 */
@Entity
@Table(name = "hr")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TransactionOfHotelReservation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "hotel_name")
    private String hotelName;

    @Column(name = "amount")
    private String amount;

    @Column(name = "date")
    private Instant date;

    @Column(name = "begin_date")
    private Instant beginDate;

    @Column(name = "end_date")
    private Instant endDate;

    @Column(name = "number_of_person")
    private Integer numberOfPerson;

    @Column(name = "receipt")
    private String receipt;

    @Column(name = "is_done")
    private Boolean isDone;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JsonIgnoreProperties("transactionOfHotelReservations")
    private User userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getCardNumber() {
        return cardNumber;
    }

    public TransactionOfHotelReservation cardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
        return this;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getHotelName() {
        return hotelName;
    }

    public TransactionOfHotelReservation hotelName(String hotelName) {
        this.hotelName = hotelName;
        return this;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getAmount() {
        return amount;
    }

    public TransactionOfHotelReservation amount(String amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Instant getDate() {
        return date;
    }

    public TransactionOfHotelReservation date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Instant getBeginDate() {
        return beginDate;
    }

    public TransactionOfHotelReservation beginDate(Instant beginDate) {
        this.beginDate = beginDate;
        return this;
    }

    public void setBeginDate(Instant beginDate) {
        this.beginDate = beginDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public TransactionOfHotelReservation endDate(Instant endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Integer getNumberOfPerson() {
        return numberOfPerson;
    }

    public TransactionOfHotelReservation numberOfPerson(Integer numberOfPerson) {
        this.numberOfPerson = numberOfPerson;
        return this;
    }

    public void setNumberOfPerson(Integer numberOfPerson) {
        this.numberOfPerson = numberOfPerson;
    }

    public String getReceipt() {
        return receipt;
    }

    public TransactionOfHotelReservation receipt(String receipt) {
        this.receipt = receipt;
        return this;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public Boolean isIsDone() {
        return isDone;
    }

    public TransactionOfHotelReservation isDone(Boolean isDone) {
        this.isDone = isDone;
        return this;
    }

    public void setIsDone(Boolean isDone) {
        this.isDone = isDone;
    }

    public String getDescription() {
        return description;
    }

    public TransactionOfHotelReservation description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUserId() {
        return userId;
    }

    public TransactionOfHotelReservation userId(User user) {
        this.userId = user;
        return this;
    }

    public void setUserId(User user) {
        this.userId = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TransactionOfHotelReservation)) {
            return false;
        }
        return id != null && id.equals(((TransactionOfHotelReservation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TransactionOfHotelReservation{" +
            "id=" + getId() +
            ", userId='" + getUserId() + "'" +
            ", cardNumber='" + getCardNumber() + "'" +
            ", hotelName='" + getHotelName() + "'" +
            ", amount='" + getAmount() + "'" +
            ", date='" + getDate() + "'" +
            ", beginDate='" + getBeginDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", numberOfPerson=" + getNumberOfPerson() +
            ", receipt='" + getReceipt() + "'" +
            ", isDone='" + isIsDone() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
