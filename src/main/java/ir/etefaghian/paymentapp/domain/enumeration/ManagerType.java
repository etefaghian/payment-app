package ir.etefaghian.paymentapp.domain.enumeration;

/**
 * The ManagerType enumeration.
 */
public enum ManagerType {
    ADMIN, MANAGER
}
