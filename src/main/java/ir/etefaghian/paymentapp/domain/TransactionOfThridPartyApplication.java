package ir.etefaghian.paymentapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

/**
 * A TransactionOfThridPartyApplication.
 */
@Entity
@Table(name = "tpa")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TransactionOfThridPartyApplication implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;



    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "amount")
    private String amount;

    @Column(name = "date")
    private Instant date;

    @Column(name = "receipt")
    private String receipt;

    @Column(name = "is_done")
    private Boolean isDone;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JsonIgnoreProperties("transactionOfThridPartyApplications")
    private User userId;

    @ManyToOne
    @JsonIgnoreProperties("transactionOfThridPartyApplications")
    private ThridPartyApplication appId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public String getCardNumber() {
        return cardNumber;
    }

    public TransactionOfThridPartyApplication cardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
        return this;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getAmount() {
        return amount;
    }

    public TransactionOfThridPartyApplication amount(String amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Instant getDate() {
        return date;
    }

    public TransactionOfThridPartyApplication date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public String getReceipt() {
        return receipt;
    }

    public TransactionOfThridPartyApplication receipt(String receipt) {
        this.receipt = receipt;
        return this;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public Boolean isIsDone() {
        return isDone;
    }

    public TransactionOfThridPartyApplication isDone(Boolean isDone) {
        this.isDone = isDone;
        return this;
    }

    public void setIsDone(Boolean isDone) {
        this.isDone = isDone;
    }

    public String getDescription() {
        return description;
    }

    public TransactionOfThridPartyApplication description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUserId() {
        return userId;
    }

    public TransactionOfThridPartyApplication userId(User user) {
        this.userId = user;
        return this;
    }

    public void setUserId(User user) {
        this.userId = user;
    }

    public ThridPartyApplication getAppId() {
        return appId;
    }

    public TransactionOfThridPartyApplication appId(ThridPartyApplication thridPartyApplication) {
        this.appId = thridPartyApplication;
        return this;
    }

    public void setAppId(ThridPartyApplication thridPartyApplication) {
        this.appId = thridPartyApplication;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TransactionOfThridPartyApplication)) {
            return false;
        }
        return id != null && id.equals(((TransactionOfThridPartyApplication) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TransactionOfThridPartyApplication{" +
            "id=" + getId() +
            ", userId='" + getUserId() + "'" +
            ", appId='" + getAppId() + "'" +
            ", cardNumber='" + getCardNumber() + "'" +
            ", amount='" + getAmount() + "'" +
            ", date='" + getDate() + "'" +
            ", receipt='" + getReceipt() + "'" +
            ", isDone='" + isIsDone() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
