package ir.etefaghian.paymentapp.repository;

import ir.etefaghian.paymentapp.domain.TransactionOfWallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TransactionOfWallet entity.
 */
@SuppressWarnings("unused")
@RepositoryRestResource
public interface TransactionOfWalletRepository extends JpaRepository<TransactionOfWallet, Long> {

}
