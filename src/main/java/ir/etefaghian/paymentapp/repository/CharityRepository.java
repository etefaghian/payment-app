package ir.etefaghian.paymentapp.repository;

import ir.etefaghian.paymentapp.domain.Charity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Charity entity.
 */
@SuppressWarnings("unused")
@RepositoryRestResource
public interface CharityRepository extends JpaRepository<Charity, Long> {

}
