package ir.etefaghian.paymentapp.repository;

import ir.etefaghian.paymentapp.domain.InternetProvider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the InternetProvider entity.
 */
@SuppressWarnings("unused")
@RepositoryRestResource
public interface InternetProviderRepository extends JpaRepository<InternetProvider, Long> {

}
