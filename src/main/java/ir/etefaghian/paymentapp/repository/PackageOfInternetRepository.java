package ir.etefaghian.paymentapp.repository;

import ir.etefaghian.paymentapp.domain.PackageOfInternet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PackageOfInternet entity.
 */
@SuppressWarnings("unused")
@RepositoryRestResource
public interface PackageOfInternetRepository extends JpaRepository<PackageOfInternet, Long> {

}
