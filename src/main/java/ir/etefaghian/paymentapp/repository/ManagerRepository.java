package ir.etefaghian.paymentapp.repository;

import ir.etefaghian.paymentapp.domain.Manager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Manager entity.
 */
@SuppressWarnings("unused")
@RepositoryRestResource
public interface ManagerRepository extends JpaRepository<Manager, Long> {

}
