package ir.etefaghian.paymentapp.repository;

import ir.etefaghian.paymentapp.domain.TransactionOfPlaneReservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the TransactionOfPlaneReservation entity.
 */
@SuppressWarnings("unused")
@RepositoryRestResource
public interface TransactionOfPlaneReservationRepository extends JpaRepository<TransactionOfPlaneReservation, Long> {


}
