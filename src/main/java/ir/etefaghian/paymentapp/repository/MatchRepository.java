package ir.etefaghian.paymentapp.repository;

import ir.etefaghian.paymentapp.domain.Match;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Match entity.
 */
@SuppressWarnings("unused")
@RepositoryRestResource
public interface MatchRepository extends JpaRepository<Match, Long> {

}
