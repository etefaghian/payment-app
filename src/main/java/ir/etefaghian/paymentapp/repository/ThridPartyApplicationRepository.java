package ir.etefaghian.paymentapp.repository;

import ir.etefaghian.paymentapp.domain.ThridPartyApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ThridPartyApplication entity.
 */
@SuppressWarnings("unused")
@RepositoryRestResource
public interface ThridPartyApplicationRepository extends JpaRepository<ThridPartyApplication, Long> {

}
